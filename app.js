const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const mongoose = require('mongoose');

const app = express();

//DB config
const db = require('./config/keys').MongoURI;

//mongoose connection
mongoose.connect(db, { useNewUrlParser: true, useUnifiedTopology: true})
    .then(() => console.log('MongoDB connected')) 
    .catch(err => console.log(err));

//EJS
app.use(expressLayouts)
app.set('view engine', 'ejs');

//Bodyparser middleware
app.use(express.urlencoded({extended: false}));

//Controllers
app.use('/', require('./controllers/index'));
app.use('/users', require('./controllers/userController'))

const PORT = process.env.PORT || 5000;

app.listen(PORT, console.log(`Server started ${PORT}`));